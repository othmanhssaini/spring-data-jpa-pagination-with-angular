package io.othman.pagination.repository;
import io.othman.pagination.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User,Long> {
    Page<User> findByContaining(String name , Pageable pageable);
}
