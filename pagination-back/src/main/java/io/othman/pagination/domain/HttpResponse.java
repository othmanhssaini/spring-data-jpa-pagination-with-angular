package io.othman.pagination.domain;
import org.springframework.http.HttpStatus;
import java.util.Map;

public class HttpResponse {
    protected String timeStamp ;
    protected int statusCode ;
    protected HttpStatus status;
    protected String message ;
    protected Map<?,?> data;
}
